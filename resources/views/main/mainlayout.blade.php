<!DOCTYPE html>
<html lang="en">
@include('layouts._header')

<body>
 @include('layouts._navbar')

 @yield('content')

 @include('layouts._footer')
    
</body>
</html>