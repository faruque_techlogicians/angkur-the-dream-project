@extends('layouts.childpagelayout')

@section('content')

<div class="letter-show-section">
    <div class="letter-show-header-number">
        বাংলা বর্ণমালা শিখুন 
    </div>

    <div class="container">
        <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="cards ">
                                <div class="face face1 h-100">
                                    <div class="content" onmouseover="playAudio1()">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <img src="image/Ka_Kha/Board.png">
                                            <!--image -->

                                            <audio id="player1">

                                                <source src="image/audio/koo.mp3">

                                            </audio>
                                        </div>
                                        <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                            <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="cards ">
                            <div class="face face1">
                                <div class="content cnt2" onmouseover="playAudio2()">
                                    <div class="row d-flex justify-content-center mb-1 pt-3">
                                        <img src="image/Ka_Kha/Kha.png">
                                        <!--image -->
                                        <audio id="player2">

                                            <source src="image/audio/kh.mp3">

                                        </audio>
                                    </div>
                                    <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                        <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                                    </div>
                                </div>
                            </div>
                        </div>

            </div>


            <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="cards ">
                            <div class="face face1 h-100">
                                <div class="content cnt3" onmouseover="playAudio3()">
                                    <div class="row d-flex justify-content-center mb-1 pt-3">
                                        <img src="image/Ka_Kha/Ga.png">
                                        <audio id="player3">

                                            <source src="image/audio/goo.mp3">

                                        </audio>
                                    </div>
                                    <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                        <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>


    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content" onmouseover="playAudio4()">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/Ka_Kha/Gha.png">
                                <!--image -->
                                <audio id="player1">

                                    <source src="image/audio/ghh.mp3">

                                </audio>
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>
    </div>


    <div class="col-lg-3 col-md-6 col-sm-12">

            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/O_A/O.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>

    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/O_A/Oi.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>

    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/O_A/Oo.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/O_A/Oou.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>

</div>

<script src="js/bornosound.js"></script>
    
@endsection