@extends('layouts.app')

@section('content')
<div class="main-canvas-section bg-light ">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="images">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">
                            <div class="container-fluid">
                                <div class="row  d-flex justify-content-center ">
                                    <video width="460" height="500" controls>
                                        <source src="video/f.mp4" type="video/mp4">
                                        
                                       
                                      </video>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 p-1">
                <div class="canvas">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div id="canvasholder" class="canvasholders canvas-one justify-content-center ">
                                <div class="row d-flix justify-content-around bg-warning">
                                    <div class="col">
                                        <input type="color" class="js-color-picker">
                                    </div>
                                    <div class="col">
                                        <input type="range" class="js-line-range border" min="5" max="20" value="5">
                                    </div>
                                    <div class="col">
                                        <label class="js-range-value">05</label>Px
                                    </div>
                                    <div class="col">
                                        <button class="btn btn-info border m-2" id="reset" type="button" onclick="reset()">reset</button>
                                    </div>
                                    <div class="col">
                                        <button class="btn badge-danger border m-2 " id="clean" type="button">Erase</button>
                                    </div>
                                    <div class="col w-25 bg-danger" id="download">


                                    </div>

                                </div>
                                <div class="row justify-content-center bg-info">
                                    <canvas class="js-paint  paint-canvas" height="490px" width="500px"><canvas>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--images-->
        </div>
        <div id="footernab" class="nav bg-warning">
                <div class="row around h-25">
                        
                        
                    </div>
        </div>
    </div>

</div>
@endsection