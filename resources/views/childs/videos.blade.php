@extends('layouts.childpagelayouttwo')
@section('content')

<div class="row d-flex justify-content-center videos_div">
    <div class="col-3">
      <video class="videos_box" controls height="100%" width="100%" poster="img/abc_song.jpg"> <source src="img/abc_song.mp4" type="video/mp4"> </video>
    </div>
  
    <div class="col-3">
      <video class="videos_box"  controls height="100%" width="100%" poster="img/fairy.png"> <source src="img/cinderella.mp4" type="video/mp4"> </video>
    </div>
  
    <div class="col-3">
      <video class="videos_box" controls height="100%" width="100%" poster="img/robin_hood.png"> <source src="img/robin_hood.mp4" type="video/mp4"> </video>
    </div>
  
  </div>
  
  <br>
  
  <div class="row d-flex justify-content-center videos_div">
    <div class="col-3">
      <video class="videos_box" controls height="100%" width="100%" poster="img/abc_song.jpg"> <source src="img/abc_song.mp4" type="video/mp4"> </video>
    </div>
  
    <div class="col-3">
      <video class="videos_box"  controls height="100%" width="100%" poster="img/fairy.png"> <source src="img/cinderella.mp4" type="video/mp4"> </video>
    </div>
  
    <div class="col-3">
      <video class="videos_box" controls height="100%" width="100%" poster="img/robin_hood.png"> <source src="img/robin_hood.mp4" type="video/mp4"> </video>
    </div>
  
  </div>
  
  <br>
  <br>
  
  <div style="border-bottom: 4px solid white; background-image: url(img/footer_image_edit.png); height: 470px; width: 100%; background-size: cover; margin: 0px; padding: 0px; border-bottom-right-radius: 2px;">
    <footer  class="page-footer font-small indigo" style="color: black; letter-spacing: 1px;">
      <br>
      <br>
      <br>
      <p style="text-align: center; color: green; font-size: 20px; border: 8px solid white;"> Sponsord by: </p>
  
      <div class="row" style="margin-right: 0px; margin-left: 0px;">
        <div class="col-2"></div>
        <div class="col-8 d-flex justify-content-center">
          <img class="sponsor" src="img/uitscc.png">
          <img class="sponsor" src="img/ict_division.png">
          <img class="sponsor" src="img/a2i.png">
        </div>
        <div class="col-2"></div>
      </div>
    
@endsection