@extends('layouts.childpagelayouttwo')
@section('content')

<div class="row story_div" style="margin-right: 0px; margin-left: 0px;">
    <div class="col-4"><img class="story_cover" src="img/ekotai_bol.jpeg"></div>
    <div class="col-8"><p class="story_para">
    এক ছিল চাষি।তার পাঁচজন ছেলে  ছিল। ছেলেরা সবসময় ঝগড়া করত।এতে চাষি  খুব দুঃখ পেত। চাষি এ ব্যাপারে ছেলেদের অনেক  বুঝাত।মাঝেমাঝে অনেক  বকাঝকা করত। <a style="color: red;" href=""> আরো পড়ো..</a> </p>
</div>
</div>
<br>

<div class="row story_div" style="margin-right: 0px; margin-left: 0px; background-color: #f4c3c3;">
    <div class="col-4"><img class="story_cover" src="img/a_dog_and_his_shadow.jpg"></div>
    <div class="col-8"><p class="story_para">
    একটি কুকুর মুখে এক টুকরা মাংস নিয়ে নদী পার হচ্ছিলো। যখন সে ব্রীজ থেকে পানির দিকে তাকালো, তখন সে হটাৎ তাকিয়ে নিজের প্রতিবিম্ব দেখতে পেলো। সে বুঝতেই <a style="color: red;" href=""> আরো পড়ো..</a> </p>
</div>
</div>
<br>

<div class="row story_div" style="margin-right: 0px; margin-left: 0px;">
    <div class="col-4"><img class="story_cover" style="border: none;" src="img/lion_and_a_mouse.jpg"></div>
    <div class="col-8"><p class="story_para">
    এক সিংহ তার গুহায় গভীর ঘুমে আচ্ছন্ন। হঠাৎ একটি ছোট ইঁদুর ছোটাছুটি করতে করতে সিংহের নাকের এক ছিদ্রে ঢুকে পড়ল। ফলে সিংহের <a style="color: red;" href=""> আরো পড়ো..</a> </p>
</div>
</div>

<div style="border-bottom: 4px solid white; background-image: url(img/bg_texture.png); height: 200px; width: 100%;
background-repeat: repeat; margin: 0px; padding: 0px; border-bottom-right-radius: 2px;"><h4 style="text-align: center; color: red; padding-top: 10px; font-weight: bold;">Upcomming story..</h4>
<img class="upcoming_item" src="img/upcoming_item.png">
</div>

<div style="border-bottom: 4px solid white; background-image: url(img/footer_image_edit.png); height: 470px; width: 100%; background-size: cover; margin: 0px; padding: 0px; border-bottom-right-radius: 2px;">
<footer  class="page-footer font-small indigo" style="color: black; letter-spacing: 1px;">
    <br>
    <br>
    <br>
    <p style="text-align: center; color: green; font-size: 20px; border: 8px solid white;"> Sponsord by: </p>

    <div class="row" style="margin-right: 0px; margin-left: 0px;">
        <div class="col-2"></div>
        <div class="col-8 d-flex justify-content-center">
            <img class="sponsor" src="img/uitscc.png">
            <img class="sponsor" src="img/ict_division.png">
            <img class="sponsor" src="img/a2i.png">
        </div>
        <div class="col-2"></div>
    </div>
    <br>
    
@endsection