@extends('layouts.app')
@section('content')
<script src="https://rawgit.com/kangax/fabric.js/master/dist/fabric.min.js"></script>
<script src=”https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js”></script>
<script src=”https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js”></script>

<div class="canvas-option-section">

        <div class="container">
            <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <a  class="join-us-button" href="/akteshikhi" style="top
                            :22px!important;">রং দিয়ে আকি</a>
                    </div>
                <!--image group-->

                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="image-select-section">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link text-denger active font-weight-bolder" id="home-tab" data-toggle="tab" href="#home1" role="tab" aria-controls="home" aria-selected="true">Background</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-denger font-weight-bolder" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="false">Things</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-denger font-weight-bolder" id="contact-tab" data-toggle="tab" href="#contact1" role="tab" aria-controls="contact" aria-selected="false">Things</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="main-canvas-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 p-1">
                        <div class="canvas">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div id="canvasholder" class="canvasholders canvas-one justify-content-center ">
    
                                        <div class="row d-flex justify-content-center bg-light text-danger m-1" id="imagers">
                                            <!-- <div class="w-50 bg-light mt-1" id="upload">
                                                <div class="upload-btn-wrapper">
                                                    <button class="btn border-secondary btns">Upload Image 
                                                        from Cmputer</button>
                                                    <input id="file" type="file" name="myfile" />
                                                </div>
                                            </div>-->
                                            <div class="w-75  " id="bash">
    
                                            </div>
                                            <div class="d-flex justify-content-center download-drag-drop-section">
                                               <button class="btn btn-primary" id="download-canvas">Download canvas as Image</button>
    
                                            </div>
    
                                        </div>
                                        <div class="row justify-content-center">
                                            <canvas id="canvas" name="fri-canvas" class="
                                                canvas" height="490px" width="500px"></canvas>
                                        </div>
    
                                    </div>
                                </div>
                               
                                
                            </div>
                        </div>
    
                    </div>
                    <!--images-->
                    <div class="col-lg-6 col-md-12 col-sm-12 ">
                        <div class="images">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">
    
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/city.png" class="img-fluid img" draggable="true" id="circle" alt="cricle">
                                            </div>
    
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/filed2.png" class=" img img-fluid" id="squre" draggable="true" alt="square">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/icebackground2.png" class="img img-fluid" draggable="true" id="triangle" alt="triangle">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/reverimg.jpg" id="rect" draggable="true" class="img img-fluid " alt="rectangle">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/mownten.png" class="img img-fluid" alt="image5" id="tree" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/filed.png" class="img img-fluid" draggable="true" id="plamtree" alt="image6">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/lake.png" draggable="true" class="img img-fluid" id="house" alt="image7">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/mownten.png" class="img img-fluid" alt="image5" id="tree" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/background/background1.png" class="img img-fluid" draggable="true" id="plamtree" alt="image6">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/tree2.png" class="img img-fluid" id="back1" alt="background1" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/tree3.png" class="img img-fluid" id="back2" alt="background2" draggable="true">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/tree.png" class="img img-fluid" id="back3" alt="background3" draggable="true">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/penguin.png" class="img img-fluid" alt="background4" id="back4" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/see-lion.png" class="img img-fluid" alt="background5" id="back5" draggable="true">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/imageedit_6_3171317748.png" class="img img-fluid" alt="background6" id="back6" draggable="true">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/icemownten.png" class="img img-fluid" alt="background7" id="back7" draggable="true">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/icehome.png" class="img img-fluid" alt="background8" id="back8" draggable="true">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact1" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/house.png" class="img img-fluid" id='man1' alt="iamge1" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/frog-png.png" class="img img-fluid" alt="image2" id="frog" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/grass.png" class="img img-fluid" alt="image3" id="dtree" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/bear.png" class="img img-fluid" alt="image4" id="pt2" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/doubleplumtree.png" class="img img-fluid" alt="image5" id="tree" draggable="true">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/plamtree3.png" class="img img-fluid" draggable="true" id="plamtree" alt="image6">
    
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 image-gap" id="images">
                                                <img src="image/forground/icemownten.png" draggable="true" class="img img-fluid" id="house" alt="image7">
    
                                            </div>
    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                    </div>
                </div>
            </div>
    
        </div>

        <div class="okhor-sikhi-footer">

                <a class="join-us-button" href="">JOIN US TODAY</a>
                <div class="okhor-sikhi-copyright">
                    copyright @ Aungkur 2019 all right reserve
                </div>
              
            </div>

            <script>

                const btndownload=document.querySelector('#download-canvas');
                const canvassave=document.querySelector('#canvas');

                btndownload.addEventListener("click", function(){
                    if(window.navigator.msSaveBlob){
                        window.navigator.msSaveBlob(canvassave.msToBlob(), "canvas-drag-drop.png");
                    }

                    else{
                        const a = document.createElement("a");

                        document.body.appendChild(a);
                        a.href = canvassave.toDataURL();
                        a.download = "canvas-drag-drop.png";
                        a.click();
                        document.body.removeChild(a);
                    }
                })
            
            </script>
    
@endsection

