@extends('layouts.childpagelayout')

@section('content')

<div class="canvas-option-section sticky-top">

      

    </div>

    <div class="main-canvas-section bg-light  mt-1 ">
            <div class="container">
                <div class="row m-2">
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/boj-giggly-park-adventure/index.html">
                                                <img src="image/gamesImage/boj-giggly-park-adventure.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/boj-giggly-park-adventure/index.html" class=" text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify mt-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/boj-giggly-park-adventure" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/chuck-chicken-magic-egg-new/index.html">
                                                <img src="image/gamesImage/chuck-chicken-magic-egg.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/chuck-chicken-magic-egg-new/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/chuck-chicken-magic-egg" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/chuck-chicken-memory-match/index.html">
                                                <img src="image/gamesImage/chuck-chicken-memory.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/chuck-chicken-memory-match/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/chuck-chicken-memory-match" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                    <!--images-->
                </div>
                <div class="row m-2">
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/alien-claw-crane/index.html">
                                                <img src="image/gamesImage/claw-crane.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/alien-claw-crane/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/alien-claw-crane" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/cross-the-bridge/index.html">
                                                <img src="image/gamesImage/cross-the-bridge.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/cross-the-bridge/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/cross-the-bridge" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/didi-and-friends-connect-the-dots/index.html">
                                                <img src="image/gamesImage/didi-and-friends-connect-the-dots.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/didi-and-friends-connect-the-dots/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/didi-and-friends-connect-the-dots" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                    <!--images-->
                </div>
                <div class="row m-2">
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/flossy-and-jim-whale-tickler/index.html">
                                                <img src="image/gamesImage/flossy-and-jim-whale-tickler.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/flossy-and-jim-whale-tickler/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/flossy-and-jim-whale-tickler" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/funny-faces/index.html">
                                                <img src="image/gamesImage/funny-faces.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/funny-faces/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/funny-faces" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/giant-hamster-run/index.html">
                                                <img src="image/gamesImage/giant-hamster-run.png">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/giant-hamster-run/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/giant-hamster-run" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                    <!--images-->
                </div>
                <div class="row m-2">
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/hidden-objects-mysterious-artifacts/index.html">
                                                <img src="image/gamesImage/hidden-objects-mysterious-artifacts.png">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/hidden-objects-mysterious-artifacts/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/hidden-objects-mysterious-artifacts" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/kuu-kuu-harajuku-stickers/index.html">
                                                <img src="image/gamesImage/kuu-kuu-harajuku-stickers.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/kuu-kuu-harajuku-stickers/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/kuu-kuu-harajuku-stickers" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/math-genius/index.html">
                                                <img src="image/gamesImage/math-genius.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/math-genius/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/math-genius" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                    <!--images-->
                </div>
                <div class="row m-2">
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/math-vs-bate/index.html">
                                                <img src="image/gamesImage/math-vs-bat.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/math-vs-bat/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/math-vs-bat" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/my-sweet-adventure/index.html">
                                                <img src="image/gamesImage/my-sweet-adventure.png">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/my-sweet-adventure/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/my-sweet-adventure" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/nom-nom-yum/index.html">
                                                <img src="image/gamesImage/nom-nom-yum.png">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/nom-nom-yum/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/nom-nom-yum" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                    <!--images-->
                </div>
                <div class="row m-2">
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/oddbods-ice-cream-fight/index.html">
                                                <img src="image/gamesImage/oddbods-ice-cream-fight.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/oddbods-ice-cream-fight/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/oddbods-ice-cream-fight" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/oddbods-soccer-challenge">
                                                <img src="image/gamesImage/oddbods-soccer-challenge.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/oddbods-soccer-challenge/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/oddbods-soccer-challenge" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/wheels-on-the-bus-sing-along/index.html">
                                                <img src="image/gamesImage/wheels-on-the-bus-sing-along.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/wheels-on-the-bus-sing-along/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/wheels-on-the-bus-sing-along" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
    
    
                    <!--images here-->
                </div>
                <div class="row m-2">
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/old-macdonald-farm/index.html">
                                                <img src="image/gamesImage/old-macdonald-farm.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/old-macdonald-farm/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/old-macdonald-farm" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/scratch-n-match-animals/index.html">
                                                <img src="image/gamesImage/scratch-n-match-animals.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/scratch-n-match-animals/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/scratch-n-match-animals" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-4 mt-3 mb-3">
                        <div class="container ">
                            <div class="card cards ">
                                <div class="face face1 h-75">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <a class="rounded-lg" href="https://cdn-factory.marketjs.com/en/smiley-shapes/index.html">
                                                <img src="image/gamesImage/smiley-shapes.jpg">
                                            </a>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-2 pl-2 pr-2">
                                            <a href="https://cdn-factory.marketjs.com/en/smiley-shapes/index.html" class="text-light font-weight-bolder justify-content-center">&nbsp; &nbsp; &nbsp; &nbsp; Play &nbsp; &nbsp; &nbsp; &nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="face face2 h-25">
    
                                    <div class="content">
                                        <div class="row d-flex justify-content-center pl-3 pr-3">
                                            <p class="text-justify m-2 "> This content belongs to marketjs.com we just create a link here</p>
                                        </div>
                                        <div class="row d-flex justify-content-center pt-5">
                                            <a href="https://www.marketjs.com/item/smiley-shapes" class="text-black-50 justify-content-center"> About the Game</a>
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                    <!--images-->
                </div>
    
                <div id="footernab" class="nav bg-warning">
                    <div class="row around h-25">
    
    
                    </div>
                </div>
            </div>
    
        </div>
        <div class="okhor-sikhi-footer" style="margin-top:150px;">

                <a class="join-us-button" href="">JOIN US TODAY</a>
                <div class="okhor-sikhi-copyright">
                    copyright @ Aungkur 2019 all right reserve
                </div>
              
        </div>
    
@endsection