@extends('layouts.childpagelayout')
@section('content')
<div class="animation-type-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="bangla-okhor" style="top:-30px!important">
                   
                  <div class="bangla-okhor-body center">
                      বাংলা  অ আ ক খ 

                     

                  </div>

                  <div class="go-page">
                      <a href="/banglaletterbook" class="go">Go To That Page </a>
                  </div>

                  <div class="go-page-bottom">
                    বাংলা অক্ষর শিখুন 

                  </div>

                  
                  </a>

                </div>

            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="english-okhor" style="top:-30px!important">
                      <div class="english-okhor-body center">
                        
                        English Letter A B C D 
                      </div>
                      <div class="go-page">
                          <a href="/letterbooks" class="go">Go To That Page </a>
                      </div>

                      <div class="go-page-bottom">
                          Learn English Letter
      
                        </div>
    
                    </div>
    
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="number-okhor" style="top:-30px!important">
                  <div class="number-okhor-body center">
                     Number  1 2 ১ ২
                  </div>
                  <div class="go-page">
                      <a href="/letterbooks" class="go">Go To That Page </a>
                  </div>
                  <div class="go-page-bottom">
                      Learn Numeric 
  
                    </div>
                </div>
        
            </div>


        </div>
    </div>

</div>

<div class="letter-show-section">
    <div class="letter-show-header-number">
        Learn Counting
    </div>

    <div class="container">
        <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="cards ">
                                <div class="face face1 h-100">
                                    <div class="content">
                                        <div class="row d-flex justify-content-center mb-1 pt-3">
                                            <img src="image/123/11.png">
                                            <!--image -->
                                        </div>
                                        <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                            <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="cards ">
                            <div class="face face1">
                                <div class="content">
                                    <div class="row d-flex justify-content-center mb-1 pt-3">
                                        <img src="image/123/12.png">
                                        <!--image -->
                                    </div>
                                    <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                        <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                                    </div>
                                </div>
                            </div>
                        </div>

            </div>


            <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="cards ">
                            <div class="face face1 h-100">
                                <div class="content">
                                    <div class="row d-flex justify-content-center mb-1 pt-3">
                                        <img src="image/123/13.png">
                                        <!--image -->
                                    </div>
                                    <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                        <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>


    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/123/14.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>
    </div>


    <div class="col-lg-3 col-md-6 col-sm-12">

            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/123/15.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>

    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/123/16.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>

    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/123/17.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="cards ">
                    <div class="face face1 h-100">
                        <div class="content">
                            <div class="row d-flex justify-content-center mb-1 pt-3">
                                <img src="image/123/18.png">
                                <!--image -->
                            </div>
                            <div class="row d-flex justify-content-center mt-5 pt-2 pl-2 pr-2">
                                <a href=" " class=" text-light font-weight-bolder justify-content-center h-25">&nbsp;&nbsp;&nbsp;লিখি&nbsp;&nbsp;&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>

</div>


    
@endsection
