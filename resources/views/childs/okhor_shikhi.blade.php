@extends('layouts.childpagelayout')

@section('content')

<div class="child-home-slider">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                 
                </ol>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="img/bg-list-vc.jpg" class="d-block w-100 child-slider-image-height-fixed" alt="...">
                    <div class="carousel-caption">
                      <div class="captoion-header">Aungkur</div>
                      <div class="caption-body">Best Learning For Your Children</div>
                    </div>
                    <div class="slider-layer">

                     </div>
                  </div>
                  <div class="carousel-item">
                    <img class="child-slider-image-height-fixed" src="img/slider.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption">
                      <div class="captoion-header">Aungkur</div>
                      <div class="caption-body">We Provide Unique Learning Method</div>
                    </div>
                    <div class="slider-layer">

                    </div>
                  </div>
                  
                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

</div>

<div class="animation-type-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="bangla-okhor">
                   
                  <div class="bangla-okhor-body center">
                      বাংলা  অ আ ক খ 

                     

                  </div>

                  <div class="go-page">
                      <a href="/banglaletterbook" class="go">Go To That Page </a>
                  </div>

                  <div class="go-page-bottom">
                    বাংলা অক্ষর শিখুন 

                  </div>

                  
                  </a>

                </div>

            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="english-okhor">
                      <div class="english-okhor-body center">
                        
                        English Letter A B C D 
                      </div>
                      <div class="go-page">
                          <a href="/letterbooks" class="go">Go To That Page </a>
                      </div>

                      <div class="go-page-bottom">
                          Learn English Letter
      
                        </div>
    
                    </div>
    
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="number-okhor">
                  <div class="number-okhor-body center">
                     Number  1 2 ১ ২
                  </div>
                  <div class="go-page">
                      <a href="/letterbooks" class="go">Go To That Page </a>
                  </div>
                  <div class="go-page-bottom">
                      Learn Numeric 
  
                    </div>
                </div>
        
            </div>


        </div>
    </div>

</div>

<div class="okhor-sikhi-footer">

  <a class="join-us-button" href="">JOIN US TODAY</a>
  <div class="okhor-sikhi-copyright">
      copyright @ Aungkur 2019 all right reserve
  </div>

</div>



    
@endsection