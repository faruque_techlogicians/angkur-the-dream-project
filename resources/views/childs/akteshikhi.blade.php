@extends('layouts.app')
@section('content')

<div class="canvas-option-section">

    <div class="container">
        <div class="row">
            <div class="image-select-section">

                <div class="col-lg-6 col-md-6 col-sm-6 d-flex justify-content-center ">
                        <a class="join-us-button" href="">রং দিয়ে আকি</a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 d-flex justify-content-center">
                        <a class="join-us-button" href="/akteshikhi_with_drag_drop">ছবি দিয়ে আকি </a>
                </div>
                
                
            </div>
        </div>
    </div>

</div>

<div class="main-canvas-section">
        <div class="container">
            <div class="row ">
                <div class="col-lg-6 col-md-12 col-sm-12 p-1">
                    <div class="canvas">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div id="canvasholder" class="canvasholders canvas-one justify-content-center ">
                                    <div class="row justify-content-center bg-warning">
                                        <input type="color" class="js-color-picker">
                                        <input type="range" class="js-line-range border" min="1" max="72" value="1">
                                        <label class="js-range-value">01</label>Px
                                        <button class="btn btn-info border m-2" id="reset" type="button" onclick="reset()">reset</button>
                                        <button class="btn badge-danger border m-2 " id="clean" type="button">Erase</button>
                                        <button class="btn btn-primary border m-2" id="Download"></button>
                                    </div>
                                    <div class="row justify-content-center bg-info">
                                        <canvas class="js-paint  paint-canvas" height="490px" width="500px"><canvas>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--images-->
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="images">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab">

                                <div class="container">
                                    <div class="row pl-lg-5 pl-md-5">
                                        <div class="row p-2">
                                                <button class="btn m-2 text-center colorbtn" id="red" style="background-color:red" type="button"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" id="yellow" style="background-color:yellow" type="button"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn image1"
                                                style="background-color:magenta"  type="button" id="green"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" style="background-color:gray" type="button" id="green"
                                                onclick=""></button>
                                        </div>
                                        <div class="row p-2">
                                                <button class="btn m-2 text-center colorbtn" style="background-color:blue" type="button" id="blue"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" style="background-color:purple" type="button" id="cyan"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" style="background-color:pink" id="magenda" type="button"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" id="orangered" style="background-color:orange" type="button"
                                                onclick=""></button>

                                        </div>
                                        <div class="row p-2">
                                                <button class="btn m-2 text-center colorbtn" id="purple" style="background-color:brown" type="button"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" id="brown" style="background-color:silver" type="button"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" id="brown" style="background-color:aqua" type="button"
                                                onclick=""></button>
                                                <button class="btn m-2 text-center colorbtn" id="brown" style="background-color:indigo" type="button"
                                                onclick=""></button>

                                        </div>                 
                                            
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>


<div class="okhor-sikhi-footer" style="margin-top:150px;">

    <a class="join-us-button" href="">JOIN US TODAY</a>
    <div class="okhor-sikhi-copyright">
        copyright @ Aungkur 2019 all right reserve
    </div>
  
</div>
    
@endsection