
	@extends('main.mainlayout')

	@section('content')

	<div class="row m-0 d-flex body-nature">
			<img src="img\mountain_5.png" class="moutnain">
	
			<div class="col-lg-4 nature-section-1">
				<img src="img\sun.png" class="sun">
				<img src="img\clouds.png" class="cloud" style="left: 180px;">
				<img src="img\tree_1.png" class="tree1">
				<img src="img\children.png" class="children">
			</div>
	
			<div class="col-lg-5 col-md-8 nature-section-2">
				<img src="img\grass_1.png" class="grass1">
				<img src="img\tree_2.png" class="tree2">
				<img src="img\board.png" class="board">
				<img src="img\clouds.png" class="cloud1">
				<img src="img\clouds_1.png" class="cloud2">
				<img src="img\grass_2.png" class="grass2">
				<img src="img\grass_2.png" class="grass2" style="right: -20px;">
			</div>
	
			<div class="col-lg-3 col-md-4 nature-section-3">
				<img src="img\clouds_1.png" class="cloud2" style="left: 220px;">
				<img src="img\tree_3.png" class="tree3">
			</div>
		</div>
	
		<div class="row menu-row m-0">
			<div class="col-lg-3 col-md-3 col-sm-3 col-3 align-self-center" style="padding: 10px;">
			<a href="{{route('childs.akteshikhi')}}">
						<img class="menu_icon mx-auto d-block" src="img\akte_shikhi.png"> 
					</a>
				 </div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-3 align-self-center" style="padding: 10px;">
				 <a href="/dekheaki"> 
					 <img class="menu_icon mx-auto d-block" src="img\likhte_shikhi_1.png"> 
				 </a> 
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-3 align-self-center" style="padding: 10px;">
				 <a href="#bolte_shikhi"> 
					 <img class="menu_icon mx-auto d-block" src="img\bolte_shikhi_1.png"> 
					</a> 
				</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-3 align-self-center" style="padding: 10px;">
			<a href="{{route('animation.okhor_shikhi')}}"> 
					 <img class="menu_icon mx-auto d-block" src="img\okkhor_shikhi_1.png"> 
					</a> 
				</div>
		</div>
		
		<div class="bangla-okkhor-sikhi-demo-section">
			<div class="bangla-okhor-sikhi-section-header d-flex justify-content-center">
				<div class="container">
					<div class="row">
						<div class="col">
								Learn Bangla Letter
						</div>
					</div>
				</div>
			
			</div>

			<div class="demo-letter">
				<div class="container">
					<div class="row  d-flex justify-content-center">
						<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="container ">
										<div class="cards ">
											<div class=" face face1">
												<div class="content">
													<div class="row d-flex justify-content-center mb-1 pt-3">
														<img src="image/Ka_Kha/Board.png">
														<!--image -->
													</div>
												</div>
											</div>
										</div>
									</div>

						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="container ">
										<div class="cards ">
											<div class=" face1">
												<div class="content">
													<div class="row d-flex justify-content-center mb-1 pt-3">
														<img src="image/Ka_Kha/Board.png">
														<!--image -->
													</div>
												</div>
											</div>
										</div>
									</div>

						</div>
						<div class="col-lg-4 col-md-6 col-sm-12">
								<div class="container ">
										<div class="cards ">
											<div class=" face1">
												<div class="content">
													<div class="row d-flex justify-content-center mb-1 pt-3">
														<img src="image/Ka_Kha/Board.png">
														<!--image -->
													</div>
												</div>
											</div>
										</div>
									</div>

						</div>
						
						
					</div>
				</div>

			</div>

			<div class="b-re-more d-flex justify-content-center">
				<a class="b-re-more-button" href="">Show More</a>

			</div>


		</div>

		<div class="english-okkhor-sikhi-demo-section ">
				<div class="english-okhor-sikhi-section-header d-flex justify-content-center">
					Learn English Letter 

				</div>
				<div class="demo-letter">
						<div class="container">
							<div class="row  d-flex justify-content-center">
								<div class="col-lg-4 col-md-6 col-sm-12">
										<div class="container ">
												<div class="cards ">
													<div class=" face face1">
														<div class="content">
															<div class="row d-flex justify-content-center mb-1 pt-3">
																<img src="image/Ka_Kha/Board.png">
																<!--image -->
															</div>
														</div>
													</div>
												</div>
											</div>
		
								</div>
								<div class="col-lg-4 col-md-6 col-sm-12">
										<div class="container ">
												<div class="cards ">
													<div class=" face1">
														<div class="content">
															<div class="row d-flex justify-content-center mb-1 pt-3">
																<img src="image/Ka_Kha/Board.png">
																<!--image -->
															</div>
														</div>
													</div>
												</div>
											</div>
		
								</div>
								<div class="col-lg-4 col-md-6 col-sm-12">
										<div class="container ">
												<div class="cards ">
													<div class=" face1">
														<div class="content">
															<div class="row d-flex justify-content-center mb-1 pt-3">
																<img src="image/Ka_Kha/Board.png">
																<!--image -->
															</div>
														</div>
													</div>
												</div>
											</div>
		
								</div>
								
								
							</div>
						</div>
		
					</div>
				<div class="b-re-more d-flex justify-content-center">
						<a class="b-re-more-button" href="">Show More</a>
		
				</div>

		</div>

		<div class="gunte-sikhi-demo-section ">
				<div class="number-okhor-sikhi-section-header d-flex justify-content-center">
					Learn Counting  
				</div>
				<div class="demo-letter">
						<div class="container">
							<div class="row  d-flex justify-content-center">
								<div class="col-lg-4 col-md-6 col-sm-12">
										<div class="container ">
												<div class="cards ">
													<div class=" face face1">
														<div class="content">
															<div class="row d-flex justify-content-center mb-1 pt-3">
																<img src="image/Ka_Kha/Board.png">
																<!--image -->
															</div>
														</div>
													</div>
												</div>
											</div>
		
								</div>
								<div class="col-lg-4 col-md-6 col-sm-12">
										<div class="container ">
												<div class="cards ">
													<div class=" face1">
														<div class="content">
															<div class="row d-flex justify-content-center mb-1 pt-3">
																<img src="image/Ka_Kha/Board.png">
																<!--image -->
															</div>
														</div>
													</div>
												</div>
											</div>
		
								</div>
								<div class="col-lg-4 col-md-6 col-sm-12">
										<div class="container ">
												<div class="cards ">
													<div class=" face1">
														<div class="content">
															<div class="row d-flex justify-content-center mb-1 pt-3">
																<img src="image/Ka_Kha/Board.png">
																<!--image -->
															</div>
														</div>
													</div>
												</div>
											</div>
		
								</div>
								
								
							</div>
						</div>
		
					</div>

				<div class="b-re-more d-flex justify-content-center">
				   <a class="b-re-more-button" href="">Show More</a>
		
				</div>

		</div>
			
	@endsection



