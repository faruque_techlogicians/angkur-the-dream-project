@extends('layouts.adminlayout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h2>Upload Letter animation</h2>

            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Bangla</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">English</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Number</a>
                    </li>
                  </ul>
                  <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                        <h2>Upload Bangla letter</h2>
                        <hr>

                    <form action="" method="post">
                        <div class="form-group">
                                <label for="my-input">Photo of Letter</label>
                                <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                            </div>
    
                        <div class="form-group">
                            <label for="my-input">Animation file of Letter</label>
                            <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                        </div>

                        <div class="form-group">
                            <label for="my-input">Animation letter voice file</label>
                            <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                        </div>

                        <div class="form-group">
                            <label for="my-input">Animation letter short description</label>
                           <textarea class="form-control" name="" id="" cols="30" rows="5"></textarea>
                        </div>

                        <button class="btn btn-success btn-block" type="submit">Upload</button>
                    </form>
                        

                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <h2>Upload English letter</h2>
                            <hr>
    
                        <form action="" method="post">
                            <div class="form-group">
                                    <label for="my-input">Photo of Letter</label>
                                    <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                                </div>
        
                            <div class="form-group">
                                <label for="my-input">Animation file of Letter</label>
                                <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                            </div>
    
                            <div class="form-group">
                                <label for="my-input">Animation letter voice file</label>
                                <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                            </div>
    
                            <div class="form-group">
                                <label for="my-input">Animation letter short description</label>
                               <textarea class="form-control" name="" id="" cols="30" rows="5"></textarea>
                            </div>
    
                            <button class="btn btn-success btn-block" type="submit">Upload</button>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <h2>Upload Number animation</h2>
                            <hr>
    
                        <form action="" method="post">
                            <div class="form-group">
                                    <label for="my-input">Photo of Letter</label>
                                    <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                                </div>
        
                            <div class="form-group">
                                <label for="my-input">Animation file of Letter</label>
                                <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                            </div>
    
                            <div class="form-group">
                                <label for="my-input">Animation letter voice file</label>
                                <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                            </div>
    
                            <div class="form-group">
                                <label for="my-input">Animation letter short description</label>
                               <textarea class="form-control" name="" id="" cols="30" rows="5"></textarea>
                            </div>
    
                            <button class="btn btn-success btn-block" type="submit">Upload</button>
                        </form>
                    </div>
                  </div>

        </div>
    </div>
</div>
    
@endsection