@extends('layouts.adminlayout')

@section('content')


<div class="upload-story">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <h2>Upload Story Admin Page</h2>

                <br>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#bangla-stories" role="tab" aria-controls="pills-home" aria-selected="true">Bangla Stories</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#english-stories" role="tab" aria-controls="pills-profile" aria-selected="false">English Stories</a>
                        </li>
                        
                      </ul>
                      <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="bangla-stories" role="tabpanel" aria-labelledby="pills-home-tab">
                           <form action="" method="post">

                             <div class="form-group">
                                 <label for="my-input">StorY Title</label>
                                 <input id="my-input" class="form-control" type="text" name="story-title">
                             </div>

                             <div class="form-group">
                              <label for="exampleFormControlSelect1">Story category</label>
                              <select class="form-control" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                              </select>
                            </div>

                             <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label" for="inputGroupFile01">Upload Story Photo</label>
                              </div>
                              <br>

                              <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Update story Body</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                             </div>

                             <div class="form-group">
                                 <label for="my-input">Moral Sentense</label>
                                 <input id="my-input" class="form-control" type="text" name="">
                             </div>
                           <button type="submit" class="btn btn-primary btn-block"> Uplaod</button>

                                
                           </form>

                        </div>
                        <div class="tab-pane fade" id="english-stories" role="tabpanel" aria-labelledby="pills-profile-tab">

                                <form action="" method="post">

                                        <div class="form-group">
                                            <label for="my-input">StorY Title</label>
                                            <input id="my-input" class="form-control" type="text" name="story-title">
                                        </div>
           
                                        <div class="custom-file">
                                               <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                               <label class="custom-file-label" for="inputGroupFile01">Upload Story Photo</label>
                                         </div>
                                         <br>
           
                                         <div class="form-group">
                                               <label for="exampleFormControlTextarea1">Update story Body</label>
                                               <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                        </div>
           
                                        <div class="form-group">
                                            <label for="my-input">Moral Sentense</label>
                                            <input id="my-input" class="form-control" type="text" name="">
                                        </div>
                                      <button type="submit" class="btn btn-primary "> Uplaod</button>
           
                                           
                                      </form>
           
                        </div>
                       
                      </div>

            </div>
        </div>
    </div>

</div>


    
@endsection