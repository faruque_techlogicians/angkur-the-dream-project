@extends('layouts.adminlayout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h2>Upload Parents Guide Post</h2>

            <form action="" method="post">
                <div class="form-group">
                    <label for="my-input">Parents Post title</label>
                    <input id="my-input" class="form-control" type="text" name="">
                </div>

                <div class="form-group">
                    <label for="my-select"> Parents Post Category</label>
                    <select id="my-select" class="form-control" name="">
                        <option>Text</option>
                        <option>Text</option>
                        <option>Text</option>
                        <option>Text</option>
                        <option>Text</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="my-input">Parents post Photo</label>
                    <input  id="my-input" class="form-control-file" type="file" name="">
                </div>

                <div class="form-group">
                    <label for="my-input">Parents Post description</label>
                    <textarea class="form-control" name="" id="" cols="30" rows="5"></textarea>
                </div>

                <button class="btn btn-primary btn-block" type="submit">Upload</button>
            </form>

        </div>
    </div>
</div>
    
@endsection