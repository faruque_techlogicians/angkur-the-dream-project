@extends('layouts.adminlayout')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="alert alert-primary d-flex justify-content-center" role="alert">
                        Message Form User (Parents)
                  </div>
                  
                  <table class="table table-dark">
                          <thead>
                            <tr>
                              <th scope="col">ID </th>
                              <th scope="col">User Name</th>
                              <th scope="col">User Email</th>
                              <th scope="col">Message Title</th>
                              <th scope="col">Read More</th>
                              <th scope="col">Delete Message</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>Mark</td>
                              <td>example @gmail.com</td>
                              <td>Hello I have some Issue</td>
                              <td><a class="btn btn-primary" href="">Read more</a></td>
                              <td><a class="btn btn-danger" href="">Delete</a></td>
                            </tr>
                            <tr>
                                  <th scope="row">1</th>
                                  <td>Mark</td>
                                  <td>example @gmail.com</td>
                                  <td>Hello I have some Issue</td>
                                  <td><a class="btn btn-primary" href="">Read more</a></td>
                                  <td><a class="btn btn-danger" href="">Delete</a></td>
                            </tr>
                            <tr>
                                  <th scope="row">1</th>
                                  <td>Mark</td>
                                  <td>example @gmail.com</td>
                                  <td>Hello I have some Issue</td>
                                  <td><a class="btn btn-primary" href="">Read more</a></td>
                                  <td><a class="btn btn-danger" href="">Delete</a></td>
                            </tr>
                          </tbody>
                  </table>

        </div>
    </div>
</div>



    
@endsection