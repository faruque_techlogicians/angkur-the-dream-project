@extends('layouts.adminlayout')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <h2>Send Message To Parents</h2>

            <form action="" method="post">
                <div class="form-group">
                    <label for="my-input">Message Title</label>
                    <input id="my-input" class="form-control" type="text" name="" placeholder="message_title">
                </div>
                <div class="form-group">
                    <label for="my-textarea">Message Body</label>
                    <textarea id="my-textarea" class="form-control" name="" rows="5" placeholder="Type message"></textarea>
                </div>

                <button type="submit" class="btn btn-danger btn-block">Send Message</button>
            </form>

        </div>
    </div>
</div>

    
@endsection