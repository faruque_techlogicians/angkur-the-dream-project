@extends('layouts.adminlayout');
@section('content')


<div class="providing-exams">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Broad Question</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Multiple choose</a>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                                  <h2>Broad Question</h2>

                                  <form action="" method="post">
                                     <div class="form-group">
                                         <label for="my-input">Exam Title</label>
                                         <input id="my-input" class="form-control" type="text" name="">
                                     </div>

                                     <div class="form-group">
                                            <label for="exampleFormControlSelect1">Story category</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                              <option>1</option>
                                              <option>2</option>
                                              <option>3</option>
                                              <option>4</option>
                                              <option>5</option>
                                            </select>
                                     </div>

                                     <div class="form-group">
                                         <label for="my-input">Question</label>
                                        <textarea class="form-control" name="" id="" cols="30" rows="5"></textarea>
                                     </div>

                                     <div class="form-group">
                                         <label for="my-input">Answer</label>
                                         <textarea class="form-control" name="" id="" cols="30" rows="5"></textarea>
                                     </div>

                                     <button class="btn btn-success btn-block" type="submit">Upload</button>

                                  </form>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <h2>Multiple Choose Questions</h2>
                                <form action="" method="post">
                                        <div class="form-group">
                                            <label for="my-input">Exam Title</label>
                                            <input id="my-input" class="form-control" type="text" name="">
                                        </div>
   
                                        <div class="form-group">
                                               <label for="exampleFormControlSelect1">Story category</label>
                                               <select class="form-control" id="exampleFormControlSelect1">
                                                 <option>1</option>
                                                 <option>2</option>
                                                 <option>3</option>
                                                 <option>4</option>
                                                 <option>5</option>
                                               </select>
                                        </div>
   
                                        <div class="form-group">
                                            <label for="my-input">MCQ Question</label>
                                            <input id="my-input" class="form-control" type="text" name="">
                                        </div>
   
                                        <div class="form-group">
                                            <label for="my-input">Answer one</label>
                                            <input id="my-input" class="form-control" type="text" name="">
                                        </div>

                                        <div class="form-group">
                                            <label for="my-input">Answer Two</label>
                                            <input id="my-input" class="form-control" type="text" name="">
                                        </div>

                                        <div class="form-group">
                                            <label for="my-input">Answer Three</label>
                                            <input id="my-input" class="form-control" type="text" name="">
                                        </div>

                                        <div class="form-group">
                                            <label for="my-input">Answer four</label>
                                            <input id="my-input" class="form-control" type="text" name="">
                                        </div>
   
                                        <button class="btn btn-success btn-block" type="submit">Upload</button>
   
                                     </form>

                            </div>
                           
                          </div>

            </div>
        </div>
    </div>

</div>
    
@endsection