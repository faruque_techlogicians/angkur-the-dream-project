@extends('layouts.adminlayout')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h2>Intro Bangladesh Post upload</h2>

            <form action="" method="post">

                <div class="form-group">
                    <label for="my-input">B Post Title</label>
                    <input id="my-input" class="form-control" type="text" name="">
                </div>

                <div class="form-group">
                        <label for="exampleFormControlSelect1">B post  category</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                 </div>


                 <div class="form-group">
                     <label for="my-input">B post Photo</label>
                     <input class="form-control" id="my-input" class="form-control-file" type="file" name="">
                 </div>

                <div class="form-group">
                    <label for="my-input">Text</label>
                   <textarea class="form-control" name="" id="" cols="30" rows="5"></textarea>
                </div>

                <button class="btn btn-success btn-block" type="submit">Upload</button>


            </form>

        </div>
    </div>
</div>


    
@endsection