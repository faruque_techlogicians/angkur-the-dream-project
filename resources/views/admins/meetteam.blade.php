<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css') }}">
    <link href="{{asset('afterlogincss/floatingdesign.css') }}" rel="stylesheet">
    <link href="{{asset('progressbar/style.css') }}" rel="stylesheet">
    <link href="{{asset('css/style.css') }}" rel="stylesheet">

    
    <link rel="stylesheet" type="text/css" href="{{asset('afterlogincss/main.css') }}">
    <link href="{{asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{asset('bootstrap/js/bootstrap.min.js') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('js\jquery-git.min.js')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('js\popper.min.js')}}">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        @font-face {
          font-family: myFont;
          src: url(rajdhani.light.ttf);
        }
      </style>
</head>

<body>
    
  <div class="meet-team-nav">
    <nav class="navbar navbar-light navbar-expand-md sticky-top">
      <div class="container">
      <a class="navbar-brand" href="{{url('/parents')}}">
      <img src="logo_ankur.png" alt="logo" class="logo">
    </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar10">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbar10">
  
          
          <ul class="navbar-nav nav-fill w-100 d-block">
            <li class="nav-item">
              <a class="nav-link" href="#"><span class="nav-text">Chat With Kids</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><span class="nav-text"> Guideline</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><span class="nav-text">Message Form Admin</span></a>
            </li>
            
           
            
           <li class="nav-item" style="float:right">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
             
          </li>
          
            
          </ul>
        </div>
      </div>
    </nav>

  </div>

  <div class="team-card">
    <div class="container">
    <div class="row">
         <div class="col-lg-3 col-md-3 col-sm-3">
             <div class="member-card">
                    <div class="team-card-header">
                            <div class="member-image">
                               <img src="img/rezwan.jpg" class="img-fluid" alt="">
          
                            </div>
          
                      </div>

                        <div class="team-card-body">
                          <div class="member-name">
                            Rezwanul Ehsan (Faruque)

                          </div>

                          <div class="member-designation">
                            Web Developer

                          </div>

                          <div class="work-done">
                             Team Leader , Backend Development , Frontend Development , 

                          </div>

                          <div class="social-media-link">
                            <div class="facebook">
                             
                              <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>


                            </div>

                            <div class="linkin">
                             <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i>
                             </a>

                            </div>
                            <div class="gmail">

                            <a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a>


                            </div>

                          </div>

                        </div>
             </div>
             

              
         </div>
         <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="member-card">
                   <div class="team-card-header">
                           <div class="member-image">
                              <img src="img/razu.jpg" class="img-fluid" alt="">
         
                           </div>
         
                     </div>

                       <div class="team-card-body">
                         <div class="member-name">
                           Mehadi Hasan (Razu)

                         </div>

                         <div class="member-designation">
                           Frontend Developer

                         </div>

                         <div class="work-done">
                            Co Leader , Frontend Development , Graphics Development , 

                         </div>

                         <div class="social-media-link">
                           <div class="facebook">
                            
                             <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>


                           </div>

                           <div class="linkin">
                            <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>

                           </div>
                           <div class="gmail">

                           <a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a>


                           </div>

                         </div>

                       </div>
            </div>
            

             
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="member-card">
                   <div class="team-card-header">
                           <div class="member-image">
                              <img src="img/modak.jpg" class="img-fluid" alt="">
         
                           </div>
         
                     </div>

                       <div class="team-card-body">
                         <div class="member-name">
                          Biplob Kumar (Modak)

                         </div>

                         <div class="member-designation">
                           Java-Script Developer

                         </div>

                         <div class="work-done">
                             Co leader , Scripting Development , Frontend Development  

                         </div>

                         <div class="social-media-link">
                           <div class="facebook">
                            
                             <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>


                           </div>

                           <div class="linkin">
                            <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>

                           </div>
                           <div class="gmail">

                           <a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a>


                           </div>

                         </div>

                       </div>
            </div>
            

             
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="member-card">
                   <div class="team-card-header">
                           <div class="member-image">
                              <img src="img/rumman.jpg" class="img-fluid" alt="">
         
                           </div>
         
                     </div>

                       <div class="team-card-body">
                         <div class="member-name">
                           Umme Rumman 

                         </div>

                         <div class="member-designation">
                            QA Tester

                         </div>

                         <div class="work-done">
                            Team Co leader , Designing , Testing, content collecting

                         </div>

                         <div class="social-media-link">
                           <div class="facebook">
                            
                             <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>


                           </div>

                           <div class="linkin">
                            <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>

                           </div>
                           <div class="gmail">

                           <a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a>


                           </div>

                         </div>

                       </div>
            </div>
            

             
        </div>
    </div>
    </div>
  </div>

 <div class="meet-team-footer">

    Design and Developed copyright @Aunkur team 
 </div>

  
</body>
</html>
