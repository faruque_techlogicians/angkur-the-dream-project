@extends('layouts.childpagelayout')

@section('content')




<div class="row m-0 p-5 board_div">
    <div class="col-4">
        <div class="row m-1">

            <a href=""><img class="childpage-image1" src="img/kids_board_1.png"></a>

        </div>
    </div>
    <div class="col-4">
        <div class="row m-1 pt-2">
            <a href="Porteshikhi.html"><img class="childpage-image2" src="img/kids_board_2.png"></a>

        </div>

    </div>
    <div class="col-4  ">
        <div class="row m-1">
            <a href="Porteshikhi.html"><img class="childpage-image3" src="img/kids_board_3.png"></a>
        </div>

    </div>
</div>

<div class="okhor-sikhi-footer">

    <a class="join-us-button" href="">JOIN US TODAY</a>
    <div class="okhor-sikhi-copyright">
        copyright @ Aungkur 2019 all right reserve
    </div>
  
</div>

@endsection
