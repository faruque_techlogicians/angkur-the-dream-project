<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/canvas.js') }}" defer></script>
    <script src="{{ asset('js/febriccanvas.js') }}" defer></script>

   

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('afterlogincss/main.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/style.css') }}">
    <link rel="stylesheet" href="{{asset('css/main.css') }}">
  
    <link rel="stylesheet" type="text/css" href="{{asset('css/canvas.css')}}">

    <link href="{{asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{asset('bootstrap/js/bootstrap.min.js') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('js\jquery-git.min.js')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('js\popper.min.js')}}">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <style>
        @font-face {
          font-family: myFont;
          src: url(rajdhani.light.ttf);
        }
      </style>
</head>
<body>
    <nav class="navbar navbar-light navbar-expand-md sticky-top">
        <div class="container">
        <a class="navbar-brand" href="{{url('/home')}}">
        <img src="logo_ankur.png" alt="logo" class="logo">
      </a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar10">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="navbar-collapse collapse" id="navbar10">
    
            
            <ul class="navbar-nav nav-fill w-100 d-block">
              <li class="nav-item">
                <a class="nav-link" href="#"><span class="nav-text">Games</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><span class="nav-text"> Bangladesh</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><span class="nav-text">General Knowledge</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><span class="nav-text">Math</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><span class="nav-text">Story</span></a>
              </li>
             
              
             <li class="nav-item" style="float:right">
                  <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
               
            </li>
            
              
            </ul>
          </div>
        </div>
      </nav>

        <main class="">
            @yield('content')
        </main>
    </div>


    <script src="https://js.pusher.com/4.4/pusher.min.js"></script>

</body>
</html>
