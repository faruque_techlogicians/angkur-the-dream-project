<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css') }}">
    <link href="{{asset('afterlogincss/floatingdesign.css') }}" rel="stylesheet">
    <link href="{{asset('progressbar/style.css') }}" rel="stylesheet">
    <link href="{{asset('css/style.css') }}" rel="stylesheet">

    
    <link rel="stylesheet" type="text/css" href="{{asset('afterlogincss/main.css') }}">
    <link href="{{asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{asset('bootstrap/js/bootstrap.min.js') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('js\jquery-git.min.js')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('js\popper.min.js')}}">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        @font-face {
          font-family: myFont;
          src: url(rajdhani.light.ttf);
        }
      </style>
</head>
<body>
    <nav class="navbar navbar-light navbar-expand-md sticky-top">
        <div class="container">
        <a class="navbar-brand" href="{{url('/parents')}}">
        <img src="logo_ankur.png" alt="logo" class="logo">
      </a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar10">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="navbar-collapse collapse" id="navbar10">
    
            
            <ul class="navbar-nav nav-fill w-100 d-block">
              <li class="nav-item">
                <a class="nav-link" href="#"><span class="nav-text">Chat With Kids</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><span class="nav-text"> Guideline</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/parentsdashboard"><span class="nav-text">Message Form Admin</span></a>
              </li>
              
             
              
             <li class="nav-item" style="float:right">
                  <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
               
            </li>
            
              
            </ul>
          </div>
        </div>
      </nav>
  

        <main class="">
            @yield('content')
        </main>

<footer>
  <div class="footer-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="contact-section">
              <form>
                  <div class="form-group form-control-color">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="email" class="form-control form-control-customize" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    
                  </div>
                  <div class="form-group form-control-color">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control form-control-customize" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                     
                  </div>
                 
                  <div class="form-group form-control-color">
                      <label for="exampleFormControlTextarea1">Message</label>
                      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Message"></textarea>
                  </div>
                  <button type="submit" class="submit">Submit</button>
                </form>
             
          </div>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <h5 style="color:white; text-align:center;">OUR DIRECT CONTACT DETAILS</h5>
          <div class="social-media-section">
               <div class="phone flex-custom">
                  <i class="fa fa-phone" aria-hidden="true"></i>


               </div>
               <div class="mail flex-custom">
                  <i class="fa fa-envelope" aria-hidden="true"></i>

               </div>
               <div class="office flex-custom">
                  <i class="fa fa-map-marker" aria-hidden="true"></i>

               </div>
          </div>

          <div class="map-section">
              <div class="mapouter"><div class="gmap_canvas">
                <iframe width="100%" height="350px" id="gmap_canvas" src="https://maps.google.com/maps?q=University%20of%20Information%20Technology%20and%20Sciences&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>

  </div>
</footer>

<div class="last-footer">
    Copyrights© 2019 Aunkur Team - Administration | All Rights Reserved.
</div>
    
</body>
</html>
