<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    
  <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  <script type="text/javascript" src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('jquery.js')}}"></script>
    
    <link rel="stylesheet" href="{{asset('admindesigncss/style.css')}}">

    


    <title>Admin Page</title>
  </head>
  <body>
    <nav class="navbar navbar-dark bg-primary fixed-top">
    <a href="/admin" class="navbar-brand navicon">Admin</a>
   
		
            <a class="btn btn-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  Logout 
            </a>
            

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
         
     
   </nav>



   <div class="sidenav">
     
   <a href="{{route('admin.upload_story')}}">Uplaoad Story</a>
   <a href="{{route('admin.providing_exam')}}">Providing Exams</a>
   <a href="{{route('admin.intro_bangladesh')}}">Intro Bangladesh</a>
   <a href="{{route('admin.upload_math')}}">Upload Math</a>
   <a href="{{route('admin.upload_parents_guide')}}">Upload Prents Guid</a>
   <a href="{{route('admin.letter_animation')}}"> Upload Letter animation</a>
   <a  href="/message-from-parents">See Parents Message</a>
   <a  href="/send-message-to-parents">Send Message To parents</a>
   <a  href="/holiday-special">Holiday Speacial</a>
   <a  href="/general-knowledge">General Knowledge</a>



      
    </div>

<br>
<div class="main">

@yield('content')


</div>


  
   <script>
      /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
      var dropdown = document.getElementsByClassName("dropdown-btn");
      var i;
      
      for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
        dropdownContent.style.display = "none";
        } else {
        dropdownContent.style.display = "block";
        }
        });
      }
      </script>

 


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    	
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>>