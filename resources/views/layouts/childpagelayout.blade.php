<!DOCTYPE html>
<html>

<head>
    <title>Child Page</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/child_videos.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="afterlogincss/main.css">
    <link rel="stylesheet" type="text/css" href="css/aunkur_home.css">
    <link rel="stylesheet" type="text/css" href="css/child_page.css">
    <link rel="stylesheet" href="css/borno.css">
    <link rel="stylesheet" type="text/css" href="js/jquery-git.min.js">
    <link rel="stylesheet" type="text/css" href="js/popper.min.js">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body class="body">
    <nav class="navbar navbar-light navbar-expand-md sticky-top">
        <div class="container">
            <a class="navbar-brand" href="/home">
                <img src="img/logo_ankur_2.png" alt="logo" class="logo">
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar10"><span class="navbar-toggler-icon"></span></button>
            <!--navbar -->
            <div class="navbar-collapse collapse" id="navbar10">
                <ul class="navbar-nav nav-fill w-100 d-block">
                    <li class="nav-item">
                        <a class="nav-link" href="/home"><span class="nav-text">Home</span></a>
                    </li>
                   
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"><span class="nav-text">
                            Class</span></a>
                        <div class="dropdown-menu dropdown-color">
                            <a href="Likhi.html" class="dropdown-item">লিখি</a>
                            <a href="Porteshikhi.html" class="dropdown-item">বই                              
                            </a>
                            <a href="index.html" class="dropdown-item"> ছবি দিয়ে আঁকি</a>
                            <a href="Likhteshikhi.html" class="dropdown-item">রং দিয়ে আঁকি</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><span class="nav-text">Contact</span></a>
                    </li>

                    <li class="nav-item" style="float:right">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                              Logout
                        </a>
      
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                     
                  </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="row branad_name m-0">
        <div class="col-3"><img class="brand_name-image1" src="img/clouds.png"></div>
        <div class="col-6 position-relative">
            <h2 class="brand_name-text">ankur.com</h2>
        </div>
        <div class="col-3"><img class="brand_name-image2" src="img/clouds.png">
            <img class="brand_name-image3" src="img/clouds.png"></div>
    </div>
    
    <div class="row menu_div m-0">
        <div class="col-1"></div>
        <div class="col-2">
            <a href="/games"><img class="menu_icon" src="img/games_icon.png"></a>
        </div>
        <div class="col-2">
            <a href="/story"> <img class="menu_icon" src="img/story_icon.png"></a>
        </div>
        <div class="col-2">
            <a href="#"> <img class="menu_icon" src="img/gift_icon.png"></a>
        </div>
        <div class="col-2">
            <a href="/videos"><img class="menu_icon" src="img/videos_icon.png"></a>
        </div>
        <div class="col-2">
            <a href="#"><img class="menu_icon" src="img/holiday_icon.png"></a>
        </div>
        <div class="col-1"></div>
    </div>


    <div class="main-manu-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 d-flex justify-content-center ">
                        <a  href="{{route('childs.akteshikhi')}}">
                                <img class="img-main-menu" src="img\akte_shikhi.png"> 
                         </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 d-flex justify-content-center ">
                        <a  href="/dekheaki"> 
                                <img class="img-main-menu" src="img\likhte_shikhi_1.png"> 
                        </a>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 d-flex justify-content-center ">
                        <a href=""> 
                                <img class="img-main-menu" src="img\bolte_shikhi_1.png"> 
                        </a> 
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 d-flex justify-content-center ">
                        <a  href="{{route('animation.okhor_shikhi')}}"> 
                                <img class="img-main-menu" src="img\okkhor_shikhi_1.png"> 
                        </a> 
                </div>
            </div>
        </div>
    
    </div>
    

    @yield('content')

    <div class="text-center text-md-left">

            <!-- Grid row -->
            <div style="background-color: white; opacity: 0.7" class="row">
        
              <!-- Grid column -->
              <div class="col-md-4 mx-auto footer_item">
        
                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">CONTACT US</h5>
        
                <ul class="list-unstyled">
                  <li>
                    Address: Baridhara, Dhaka-1216
                  </li>
                  <li>
                    Phone: 01759588288
                  </li>
                  <li>
                    E-mail: ankur-bd.org@gmail.com
                  </li>
                  <li>
                    <a class="text-dark" href="www.ankur-org.bd">Website: ankur-bd.org</a>
                  </li>
                </ul>
        
              </div>
              <!-- Grid column -->
        
              <hr class="clearfix w-100 d-md-none">
        
              <!-- Grid column -->
              <div class="col-md-3 mx-auto footer_item">
        
                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">IMPORTANT LINKS</h5>
        
                <ul class="list-unstyled">
                  <li>
                    <a class="text-dark" href="#!">Beautiful Bangladesh</a>
                  </li>
                  <li>
                    <a class="text-dark" href="#!">National Science Museum</a>
                  </li>
                  <li>
                    <a class="text-dark" href="#!">History of Bangladesh</a>
                  </li>
                </ul>
        
              </div>
              <!-- Grid column -->
        
              <hr class="clearfix w-100 d-md-none">
        
              <!-- Grid column -->
              <div class="col-md-3 mx-auto footer_item">
        
                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">OUR SERVICES</h5>
        
                <ul class="list-unstyled">
                  <li>
                    <a class="text-dark" href="#!">Math Tricks</a>
                  </li>
                  <li>
                    <a class="text-dark" href="#!">Story Books</a>
                  </li>
                  <li>
                    <a class="text-dark" href="#!">Kids Toys</a>
                  </li>
                </ul>
        
              </div>
              <!-- Grid column -->
        
              <hr class="clearfix w-100 d-md-none">
        
              <!-- Grid column -->
              <div class="col-md-2 mx-auto footer_item">
        
                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">GET TOUCH IN</h5>
        
                <ul class="list-unstyled">
                  <li>
                    <a class="text-dark" href="#!">Facebook</a>
                  </li>
                  <li>
                    <a class="text-dark" href="#!">Twitter</a>
                </ul>
        
              </div>
              <!-- Grid column -->
        
            </div>
            <!-- Grid row -->
        
          </div>
          <!-- Footer Links -->
        
          <!-- Copyright -->
          <div style="background-color: #EDEAEA; opacity: 0.8; position: fixed; bottom: 0px; width: 100%; font-weight: bold;" class="footer-copyright text-center py-3"> ©ankur-bd.org
          </div>
          <!-- Copyright -->
        
        </footer>
        <!-- Footer -->
    </div>
</body>

</html>