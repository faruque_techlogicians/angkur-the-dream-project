@extends('layouts.parentslayout')
@section('content')

<div class="p-msg-dashboard-nav">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <form action="" method="post">

                        <div class="input-group">
                                <input class="form-control" type="text" name="" placeholder="Search - Message - with - message -title" aria-label="Recipient's " aria-describedby="my-addon">
                                <div class="input-group-append">
                                    <button class="btn btn-primary " type="submit">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        Search Message
                                    </button>
                                </div>
                            </div>

                </form>
               

            </div>
        </div>
    </div>

</div>


<div class="p-msg-show-msg-section">
    <div class="sh-msg-sec-header">
        Your Message From Admin 

    </div>

    <div class="sh-msg-body-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                        <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th scope="col">Message Title</th>
                                    <th scope="col">Message Body </th>
                                    <th scope="col">Read More</th>
                                    <th scope="col">Delete</th>
                                    <th scope="col">Time</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    
                                    <td> Hello From First Title 
                                            <span class="badge  badge-danger">New</span>
                                    </td>
                                    <td>Hello this is the First Message From admin it is send for you </td>
                                    <td><a class="btn btn-success" href="">Read More</a></td>
                                    <td><a  class="btn btn-danger" href="">Delete Message</a></td>
                                    <td>12:30 pm </td>
                                  </tr>
                                  <tr>
                                    
                                        <td> Hello From First Title
                                                <span class="badge  badge-danger">New</span>
                                        </td>
                                        <td>Hello this is the First Message From admin it is send for you </td>
                                        <td><a class="btn btn-success" href="">Read More</a></td>
                                        <td><a  class="btn btn-danger" href="">Delete Message</a></td>
                                        <td>12:30 pm </td>
                                      </tr>


                                      <tr>
                                    
                                            <td> Hello From First Title
                                                    <span class="badge  badge-danger">New</span>
                                            </td>
                                            <td>Hello this is the First Message From admin it is send for you </td>
                                            <td><a class="btn btn-success" href="">Read More</a></td>
                                            <td><a  class="btn btn-danger" href="">Delete Message</a></td>
                                            <td>12:30 pm </td>
                                     </tr>



                                     <tr>
                                    
                                            <td> Hello From First Title
                                                    <span class="badge  badge-danger">New</span>
                                            </td>
                                            <td>Hello this is the First Message From admin it is send for you </td>
                                            <td><a class="btn btn-success" href="">Read More</a></td>
                                            <td><a  class="btn btn-danger" href="">Delete Message</a></td>
                                            <td>12:30 pm </td>
                                    </tr>


                                    <tr>
                                    
                                            <td> Hello From First Title
                                                    <span class="badge  badge-danger">New</span>
                                            </td>
                                            <td>Hello this is the First Message From admin it is send for you </td>
                                            <td><a class="btn btn-success" href="">Read More</a></td>
                                            <td><a  class="btn btn-danger" href="">Delete Message</a></td>
                                            <td>12:30 pm </td>
                                     </tr>
                                </tbody>
                              </table>

                </div>
            </div>
        </div>



    </div>
    <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
              </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link" href="#">Next</a>
              </li>
            </ul>
          </nav>

</div>


    
@endsection