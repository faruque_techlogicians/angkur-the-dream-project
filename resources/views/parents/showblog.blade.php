@extends('layouts.parentslayout')
@section('content')
<div class="home-slider-blog-parent">
    <div class="left-side-blog-parent">
            <img src="img/home-girl-chilling.png" class="img-fluid" alt="">
    </div>

    <div class="center-side-blog-parent">
          <img src="img/rules-child-chemist.png" class="img-fluid" alt="">
    </div>

    <div class="right-side-blog-parent">
            <img src="img/home-girl-painting.png" class="img-fluid" alt="">
    </div>

</div>

<div class="post-blog-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12">
                 <div class="blog-sec-header">
                     Blog Post Goes Here 
                 </div>

                   <div class="blog-sec-body">
                        Lorem Ipsum is simply dummy text of 
                        
                        the printing and typesetting industry
                        . Lorem Ipsum has been the industry's
                         standard dummy text ever since the 
                         1500s, when an unknown printer took
                          a galley of type and scrambled it 
                          to make a type specimen book. 
                          It has survived not only five 
                          centuries, but also the leap into 
                          electronic typesetting, remaining 
                          essentially unchanged. It was 
                          popularised in the 1960s with the 
                          release of Letraset sheets containing 
                          Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                   </div>
            </div>

            <div class="col-lg-5 col-md-12 col-sm-12">
                <div class="blog-post-body-img-section">
                        <img src="img/1.jpeg" class="img-fluid" alt="">
                </div>
               

            </div>
        </div>
    </div>

</div>

<div class="related-blog-post-section">
 <div class="container">
     <div class="row">

        <div class="col-12">
            <div class="rel-blog-post-header">
                   Related Post
            </div>

        </div>
         <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="iteam-chart-parents">
                        <div class="chart-header">
                            <div class="image">
                                <img src="img/1.jpeg" class="img-fluid" alt="">
        
                            </div>
        
                        </div>
                        <div class="chart-body">
                            <b>Lorem ipsum dolor sit amet
                                    Lorem ipsum dolor sit ametLorem ipsum dolor sit amet
                            </b>
        
                        </div>
        
                        <div class="chart-footer">
                            <a class="read-more" href="/blogedit">Read More</a>
                            
        
                        </div>
        
                        
        
                    </div>       
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="iteam-chart-parents">
                        <div class="chart-header">
                            <div class="image">
                                <img src="img/1.jpeg" class="img-fluid" alt="">
        
                            </div>
        
                        </div>
                        <div class="chart-body">
                            <b>Lorem ipsum dolor sit amet
                                    Lorem ipsum dolor sit ametLorem ipsum dolor sit amet
                            </b>
        
                        </div>
        
                        <div class="chart-footer">
                            <a class="read-more" href="/blogedit">Read More</a>
                            
        
                        </div>
        
                        
        
                    </div>       
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="iteam-chart-parents">
                        <div class="chart-header">
                            <div class="image">
                                <img src="img/1.jpeg" class="img-fluid" alt="">
        
                            </div>
        
                        </div>
                        <div class="chart-body">
                            <b>Lorem ipsum dolor sit amet
                                    Lorem ipsum dolor sit ametLorem ipsum dolor sit amet
                            </b>
        
                        </div>
        
                        <div class="chart-footer">
                            <a class="read-more" href="/blogedit">Read More</a>
                            
        
                        </div>
        
                        
        
                    </div>       
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="iteam-chart-parents">
                        <div class="chart-header">
                            <div class="image">
                                <img src="img/1.jpeg" class="img-fluid" alt="">
        
                            </div>
        
                        </div>
                        <div class="chart-body">
                            <b>Lorem ipsum dolor sit amet
                                    Lorem ipsum dolor sit ametLorem ipsum dolor sit amet
                            </b>
        
                        </div>
        
                        <div class="chart-footer">
                            <a class="read-more" href="/blogedit">Read More</a>
                            
        
                        </div>
        
                        
        
                    </div>       
         </div>
         <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="iteam-chart-parents">
                        <div class="chart-header">
                            <div class="image">
                                <img src="img/1.jpeg" class="img-fluid" alt="">
        
                            </div>
        
                        </div>
                        <div class="chart-body">
                            <b>Lorem ipsum dolor sit amet
                                    Lorem ipsum dolor sit ametLorem ipsum dolor sit amet
                            </b>
        
                        </div>
        
                        <div class="chart-footer">
                            <a class="read-more" href="/blogedit">Read More</a>
                            
        
                        </div>
        
                        
        
                    </div>       
         </div>
     </div>
 </div>

</div>
    
@endsection