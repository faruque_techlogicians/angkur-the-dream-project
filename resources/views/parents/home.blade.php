@extends('layouts.parentslayout')
@section('content')
<div class="body-image-parentpage">
 
<div class="caption-parent-homeimage">
    <b style="font-size:32px; font-family:sans-serif;">Trusted By Educator And Parents </b><br>
   <br><br><br>
    
<a href="{{ route('admin.meetteam')}}" class="meet-the-team">Meet Our Team</a>

</div>

</div>

<div class="container">
<div class="row">
<div class="col-md-12">
        <div class="header-parent">
                
            <h2>Recent Blog Activities For You</h2>
    
        </div>

</div>

</div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="iteam-chart-parents">
                <div class="chart-header">
                    <div class="image">
                        <img src="img/unnamed.jpg" class="img-fluid" alt="">

                    </div>

                </div>
                <div class="chart-body">
                    <b>গুছিয়ে লিখার অভ্যাস গড়ে তুলি ছোট বয়স থেকেই
                        <br>
                        অভিভাবকদের সব সময়কার চাওয়া আমাদের সন্তানেরা যেন একটু গোছালো হয়।তার কথা-লিখা বা আচরণে সে যেন গোছানো স্বভাবের হয়। 
                    </b>



                </div>

                <div class="chart-footer">
                    <a class="read-more" href="/blogedit">Read More</a>
                    

                </div>

                

            </div>

        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="iteam-chart-parents">
                <div class="chart-header">
                    <div class="image">
                        <img src="img/unnamed1.jpg" class="img-fluid" alt="">

                    </div>

                </div>
                <div class="chart-body">
                    <b>শিশুরা ছোটবেলায় অনেক কিছুই হতে চায়। ডাক্তার, ইঞ্জিনিয়ার, সায়েন্টিস্ট, পাইলট ইত্যাদি। স্বপ্ন লিখার এই নোটবুকে শিশুকে নিয়ে তার স্বপ্নগুলো লিখুন। তাকে তার ইচ্ছেগুলো লিখতে উৎসাহিত করুন।
                    </b>

                </div>

                <div class="chart-footer">
                        <a class="read-more" href="/blogedit">Read More</a>

                </div>

               
            </div>

        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="iteam-chart-parents">
                    <div class="chart-header">
                        <div class="image">
                            <img src="img/unnamed2.jpg" class="img-fluid" alt="">
    
                        </div>
    
                    </div>
                    <div class="chart-body">
                        <b>শিশুরা সকল বিষয়েই অনেক আগ্রহী এবং উৎসাহী হয়। তাদের অনেক প্রশ্ন থাকে। এবং জানতে চাওয়ারও তাদের শেষ নেই। গবেষণা বলে যে একটি ৪ বছরের শিশু দিনে গড়ে ৪৩৭টি প্রশ্ন করে এবং এখানে বেশিরভাগ প্রশ্ন থাকে, “কেন?”
                        </b>
    
                    </div>
    
                    <div class="chart-footer">
                            <a class="read-more" href="/blogedit">Read More</a>
    
                    </div>
    
                    
    
                </div>
    
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="iteam-chart-parents">
                    <div class="chart-header">
                        <div class="image">
                            <img src="img/unnamed3.jpg" class="img-fluid" alt="">
    
                        </div>
    
                    </div>
                    <div class="chart-body">
                        <b>শিশুরা আঁকতে অনেক পছন্দ করে।তাই এমন একটি আঁকার খাতা তার সাথে থাকবে যেখানে সে সারাদিন যা কিছু আঁকতে চাইবে তার সবকিছু এঁকে রাখবে। আঁকিবুঁকি, কাটাকাটি যা ইচ্ছে তা করতে পারে এই খাতায়।
                        </b>
    
                    </div>
    
                    <div class="chart-footer">
                            <a class="read-more" href="/blogedit">Read More</a>
    
                    </div>
    
                   
                </div>
    
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="iteam-chart-parents">
                    <div class="chart-header">
                        <div class="image">
                            <img src="img/unnamed4.jpg" class="img-fluid" alt="">
    
                        </div>
    
                    </div>
                    <div class="chart-body">
                        <b>আমরা চাই আমাদের শিশুদের নিয়ে আনন্দময় মুহুর্তগুলোকে প্রিজার্ভ করে রাখতে। এমন একটি নোটগুক আপনি আপনার শিশুর সাথে বানাতে পারেন যেখানে দুজন একসাথে এই কাজটি করতে পারেন
                        </b>
    
                    </div>
    
                    <div class="chart-footer">
                       <a class="read-more" href="/blogedit">Read More</a>
    
                    </div>
    
                    
                </div>
    
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="iteam-chart-parents">
                    <div class="chart-header">
                        <div class="image">
                            <img src="img/unnamed4.jpg" class="img-fluid" alt="">
    
                        </div>
    
                    </div>
                    <div class="chart-body">
                        <b>Lorem ipsum dolor sit amet
                                Lorem ipsum dolor sit ametLorem ipsum dolor sit amet
                        </b>
    
                    </div>
    
                    <div class="chart-footer">
                       <a class="read-more" href="/blogedit">Read More</a>
    
                    </div>
    
                    
    
                </div>
    
        </div>
    </div>
</div>







    
@endsection