const paintCanvas = document.querySelector('.js-paint');
const colorButtons = document.querySelectorAll('.colorbtn');

const context = paintCanvas.getContext('2d');
context.lineCap = 'round';
var strockcolr = '#000';

var clickcheck = false;

const colorPicker = document.querySelector('.js-color-picker');
context.strokeStyle = strockcolr;
colorPicker.addEventListener('change', event => {
  if (clickcheck === false) {
    context.strokeStyle = event.target.value;
  }
});

const lineWidthRange = document.querySelector('.js-line-range');
const lineWidthLabel = document.querySelector('.js-range-value');

lineWidthRange.addEventListener('input', event => {
  const width = event.target.value;
  lineWidthLabel.innerHTML = width;
  context.lineWidth = width;
});

let x = 0, y = 0;
let isMouseDown = false;

const stopDrawing = () => { isMouseDown = false; }
const startDrawing = event => {
  isMouseDown = true;
  [x, y] = [event.offsetX, event.offsetY];
}
const drawLine = event => {
  if (isMouseDown) {
    const newX = event.offsetX;
    const newY = event.offsetY;
    context.beginPath();
    context.moveTo(x, y);
    context.lineTo(newX, newY);
    context.stroke();
    [x, y] = [newX, newY];
  }
}
function reset() {
  context.clearRect(0, 0, context.canvas.width, context.canvas.height);
}

document.getElementById('clean').addEventListener('click', function (e) {
  context.strokeStyle = "#fff";
});

// function clear() {
//   console.log("button clickC");
//   context.strokeStyle = '#fff'
// }

colorButtons.forEach(function (item, index) {
  item.addEventListener('click', changeStrokeColor);
});

function changeStrokeColor(evt) {
  context.strokeStyle = this.style.backgroundColor;
}

paintCanvas.addEventListener('mousedown', startDrawing);
paintCanvas.addEventListener('mousemove', drawLine);
paintCanvas.addEventListener('mouseup', stopDrawing);
paintCanvas.addEventListener('mouseout', stopDrawing);
// canvas download

var link = document.createElement('a');

link.innerHTML = 'Download';

link.addEventListener('click', function(ev) {

    link.href = paintCanvas.toDataURL();

    link.download = "Rong tuli chobi.png";

}, false);

document.getElementById('Download').appendChild(link);