<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExamCategoryIdToBroadQuestionExams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('broad_question_exams', function (Blueprint $table) {
            $table->integer('exam_category_id')->nullable()->after('b_answer')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('broad_question_exams', function (Blueprint $table) {
            $table->dropColumn('exam_category_id');
        });
    }
}
