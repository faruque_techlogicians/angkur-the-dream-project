<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntBdPostCategoryIdToIntroBangladeshes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('intro__bangladeshes', function (Blueprint $table) {
            $table->integer('int_bd_post_category_id')->nullable()->after('postphoto')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('intro__bangladeshes', function (Blueprint $table) {
            $table->dropColumn('int_bd_post_category_id');
        });
    }
}
