<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultipleQuestionExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiple_question_exams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('examtitle');
            $table->string('m_question');
            $table->string('ansone');
            $table->string('anstwo');
            $table->string('ansthree');
            $table->string('ansfour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiple_question_exams');
    }
}
