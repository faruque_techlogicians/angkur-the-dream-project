<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentsGuidePostCategoryIdToParentsGuides extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parents_guides', function (Blueprint $table) {
            $table->integer('p_guide_post_category_id')->nullable()->after('pposttitle')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parents_guides', function (Blueprint $table) {
            $table->dropColumn('p_guide_post_category_id');
        });
    }
}
