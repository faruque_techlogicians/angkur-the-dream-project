<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnglishLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('english_letters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('letterphoto')->nullable();
            $table->string('animationfile');
            $table->string('voicefile');
            $table->string('lettershortdescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('english_letters');
    }
}
