<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminController extends Controller
{
    public function __contruct(){
        $this->middleware('guest:admin',['except'=>'adminlogout']);
    }

    public function showadminlogin(){
        return view('admins.adminlogin');
    }


  public function adminlogin(Request $request){
      //validate the admin

      $this->validate($request,[
         'email'=>'required|email',
         'password'=>'required|min:6'
      ]);

        if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
        {
            return redirect()->intended(route('admin.home'));

        }

        return redirect()->back()->withInput($request->only('email','remember'));

  }





    public function adminlogout(){

    }
}
