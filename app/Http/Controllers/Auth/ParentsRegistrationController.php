<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Userparent;
use Auth;

class ParentsRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showlogin(){
        return view('auth.parentslogin');
    }
    public function create()
    {
        return view('auth.parentssignup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $userparents=$request->validate([

        'name'=>'required',
        'email'=>'required|email',
        'password'=>'required|min:6',


       ]);
          // store in the database
          $userparents = new Userparent;
          $userparents->name = $request->input('name');
          $userparents->email = $request->input('email');
          $userparents->password=Hash::make($request->input('password'));
          $userparents->save();
          return redirect('/loginparents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    //login parents user

    public function loginparents( Request $request){

        //validate the userparents
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6'

        ]);


        if(Auth::guard('userparent')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
        {
            return redirect()->intended(route('parents.home'));
        }

        return redirect()->back()->withInput($request->only('email','remember'));
    }
}
