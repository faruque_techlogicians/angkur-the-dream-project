<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function games(){
        return view('childs.games');
    }

    public function story(){
        return view('childs.story');
    }

    public function videos(){
        return view('childs.videos');
    }

    public function storypage(){
        return view('childs.storypage');
    }
}
