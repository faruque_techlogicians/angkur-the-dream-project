<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    
      public function aunkurhome(){
          return view('welcome');
      }

      public function parentslogin(){
          return view('auth.parentslogin');
      }

      public function kidslogin(){
          return view('auth.login');
      }

      public function kidssignup(){
          return view('auth.kidssignup');
      }

      public function parentssignup(){
          return view('auth.parentssignup');
      }

      public function meetourteam(){
          return view('admins.meetteam');
      }

}
