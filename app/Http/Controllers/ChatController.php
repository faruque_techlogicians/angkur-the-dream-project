<?php

namespace App\Http\Controllers;


use App\Events\MessageSent;
use Illuminate\Http\Request;
use App\Message;
use App\User;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{

    public function __construct(){
         $this->middleware('auth');
    }
    

    public function index(){
        return view('chats.messagechat');

    }

   

    public function send(){
        $message= "Rezwan Faruque is testing pusher";
        $user=User::find(Auth::id());
        event(new Messagesent($message,$user));
       
    }
}
