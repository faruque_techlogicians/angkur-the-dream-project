<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminhomeController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth:admin'); 
    }


    public function adminhome(){
        return view('admins.adminhomepage');
    }

    public function upload_story(){
        return view('admins.adminupload_story');
    }

    public function providing_exam(){
        return view('admins.adminproviding_exam');

    }

    public function upload_math(){

        return view('admins.adminupload_math');

    }

    public function upload_parents_guide(){

        return view('admins.adminupload_parents_guide');
    }

    public function intro_bangladesh(){
        return view('admins.intro_bangladesh');
    }

    public function upload_letter_animation(){
        return view('admins.upload_animation');
    }

    public function send_message_to_parents(){
        return view('admins.send_message_to_parents');
    }

    public function message_from_parents(){
        return view('admins.message_from_parents');
    }


   public function holiday_special(){
       return view('admins.holiday_special');
   }

   public function general_knowledge(){
       return view('admins.general_knowledge');
   }
}
