<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Message routes


Route::get('/message','ChatController@index')->name('index.message');

Route::get('/chatmessage','ChatController@send')->name('message.send');
//meet team page route
Route::get('/meettheteam','PagesController@meetourteam')->name('admin.meetteam');

//aunkur home page route
//adminlogin route
Route::get('/general-knowledge','AdminhomeController@general_knowledge')->name('admin.general-knowledge');
Route::get('/holiday-special','AdminhomeController@holiday_special')->name('admin.holiday-special');
Route::get('/message-from-parents','AdminhomeController@message_from_parents')->name('admin.message-from-parents');
Route::get('/send-message-to-parents','AdminhomeController@send_message_to_parents')->name('admin.send_message_to_parents');
Route::get('/letteranimation','AdminhomeController@upload_letter_animation')->name('admin.letter_animation');
Route::get('/introbangladesh','AdminhomeController@intro_bangladesh')->name('admin.intro_bangladesh');
Route::get('/uploadparentsguide','AdminhomeController@upload_parents_guide')->name('admin.upload_parents_guide');
Route::get('/uploadmaths','AdminhomeController@upload_math')->name('admin.upload_math');
Route::get('/providingexams','AdminhomeController@providing_exam')->name('admin.providing_exam');
Route::get('/uploadstory','AdminhomeController@upload_story')->name('admin.upload_story');
Route::get('/adminlogin','Auth\AdminController@showadminlogin')->name('adminlogin');
Route::post('/adminlogin','Auth\AdminController@adminlogin')->name('adminlogin.submit');
Route::get('/admin','AdminhomeController@adminhome')->name('admin.home');


// 

// childs route all
Route::get('/storypage','HomeController@storypage')->name('childs.storypage');
Route::get('/videos','HomeController@videos')->name('childs.videos');
Route::get('/dekheaki','AkteshikhiController@index_three')->name('childs.dekhaaki');
Route::get('/banglaletterbook','LetterBookController@banglaletterbook')->name('childs.banglaletterbook');
Route::get('/story','HomeController@story')->name('childs.story');
Route::get('/letterbooks','LetterBookController@index')->name('childs.letterbooks');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/games','HomeController@games')->name('childs.game');
Route::get('/akteshikhi_with_drag_drop','AkteshikhiController@index_two')->name('childs.canvas_drag_drop');
Route::get('/akteshikhi','AkteshikhiController@index')->name('childs.akteshikhi');
Route::get('/okhorshikhi','Okhor_shikhiController@index')->name('animation.okhor_shikhi');
//
//parents registration route
//parents home page route
Route::get('/parentsdashboard','ParentshomeController@parentsmsgdashboard')->name('parents.dashboard');
Route::get('/blogedit','ParentshomeController@parentsblogpost')->name('parents.blogpost');

Route::get('/parents','ParentshomeController@parentshome')->name('parents.home');

//
 Route::resource('parentsregistraion', 'Auth\ParentsRegistrationController');

 Route::get('/parentssignup','Auth\ParentsRegistrationController@create');

 Route::get('/loginparents','Auth\ParentsRegistrationController@showlogin')->name('loginparents');

 //login parests route

 Route::post('/loginparents','Auth\ParentsRegistrationController@loginparents')->name('parentslogin.submit');
  
//

Route::get('/','PagesController@aunkurhome');

Route::get('/loginkids','PagesController@kidslogin');
ROute::get('/signupkids','PagesController@kidssignup');
ROute::get('/signupparents','PagesController@parentssignup');
Auth::routes();




